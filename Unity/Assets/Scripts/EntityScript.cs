﻿using UnityEngine;
using System.Collections;

public class EntityScript : MonoBehaviour {
	Rigidbody2D rb2d;
	Animator animator;
	SpriteScript sprite;
	
	public float speed = 100f;
	public float gravity = 50f; 
	public float jumpForce = 100f;
	
	public bool onGround = false;
	float fallTimer = 0f;
	public float jumpOnFallTime = 0.1f;
	
	public AudioClip jumpAudio;
	public AudioClip hurtAudio;
	public AudioClip dieAudio;
    new AudioSource audio;
	
	public int health = 1;
	int maxHealth;
	
	public int score = 1;
	
	float noHurtTimer = 0f;
	public float noHurtTime = 0.1f;
	
	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
		animator = GetComponentInChildren<Animator>();
		sprite = GetComponentInChildren<SpriteScript>();
        audio = GetComponent<AudioSource>();
		
		maxHealth = health;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (rb2d.bodyType == RigidbodyType2D.Static) return;

		if(!onGround){
			rb2d.velocity += new Vector2(0, -gravity);
			if(fallTimer > 0){
				fallTimer -= Time.deltaTime;
			}
		}
		
		if(rb2d.velocity.x > 0)
			sprite.dir = 1;
		else if(rb2d.velocity.x < 0)
			sprite.dir = -1;
			
		if(onGround && health <= 0){
			enabled = false;
            //GetComponent<Rigidbody2D>().isKinematic = true;
            //GetComponent<Collider2D>().enabled = false;
            rb2d.gravityScale = 5f;
		}	
		
		if(onGround){
			animator.SetBool("Walk", Mathf.Abs(rb2d.velocity.x) > 0.1);
		}
		
		if(noHurtTimer > 0f){
			noHurtTimer -= Time.deltaTime;
			if(noHurtTimer < 0f) noHurtTimer = 0f;
		}
	}
	
	void OnGroundCollide(Collider2D other){
		if(other.tag == "Block"){
			onGround = true;

			animator.SetBool("Jump", false);
		}
	}
	
	void OnFall(){
		if(onGround) fallTimer = jumpOnFallTime;
		onGround = false;
		animator.SetBool("Jump", true);
		
	}
	
	public void Move(Vector2 move){
		rb2d.velocity += move * speed;
		
		
	}
	
	public void Jump(){
		if(onGround || fallTimer > 0){
			rb2d.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
			onGround = false;
			fallTimer = 0f;

            audio.PlayOneShot(jumpAudio);
						
			animator.SetBool("Jump", true);
		}
	}
	
	public void Push(Vector3 fromPos, float force){
		rb2d.velocity += (new Vector2(
			transform.position.x - fromPos.x, 
			transform.position.y - fromPos.y
		).normalized + Vector2.up) * force;
	}
	
	public bool Hurt(int value){
		if(noHurtTimer > 0) return false;
		if(health <= 0) return false;
		
		health -= value;
		
		if(health <= 0){
			animator.SetTrigger("Die");
            audio.PlayOneShot(dieAudio);
			
			PlayerScript.Instance.AddToScore(score);
			SendMessage("OnKilled", SendMessageOptions.DontRequireReceiver);

            //rb2d.bodyType = RigidbodyType2D.Static;
            Vector2 vel = rb2d.velocity;
            vel.x = 0;
            vel.y = 0;
            rb2d.velocity = vel;
            this.gameObject.layer = LayerMask.NameToLayer("Dead");

		}
		else {
			animator.SetTrigger("Hurt");
			
			audio.PlayOneShot(hurtAudio);
			
			SendMessage("OnHurted", SendMessageOptions.DontRequireReceiver);
		}
		
		noHurtTimer = noHurtTime;
		return true;
	}
	
	public void SetNoHurt(){
		noHurtTimer = noHurtTime;
	}
	
	public void Heal(int value){
		health += value;
		if(health > maxHealth)
			health = maxHealth;
	}
	
	
}
