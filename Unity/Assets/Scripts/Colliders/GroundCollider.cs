﻿using UnityEngine;
using System.Collections;

public class GroundCollider : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D other) {
		SendMessageUpwards("OnGroundCollide", other, SendMessageOptions.DontRequireReceiver);
	}
		
	void OnTriggerStay2D(Collider2D other) {
		SendMessageUpwards("OnGroundCollide", other, SendMessageOptions.DontRequireReceiver);
	}
	
	void OnTriggerExit2D(Collider2D other) {
		SendMessageUpwards("OnFall", SendMessageOptions.DontRequireReceiver);
	}
}
