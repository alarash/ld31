﻿using UnityEngine;
using System.Collections;

public enum EdgeType {
	JUMP,
	WALL
}

public class EdgeTrigger : MonoBehaviour {

	public EdgeType type = EdgeType.JUMP;
	public float dir = 1f; 

	void OnTriggerEnter2D(Collider2D other) {
		other.SendMessage("OnEdgeCollide", this, SendMessageOptions.DontRequireReceiver);
	}
	
	void OnTriggerStay2D(Collider2D other) {
		if(type == EdgeType.WALL)
			other.SendMessage("OnEdgeCollide", this, SendMessageOptions.DontRequireReceiver);
	}
	
}
