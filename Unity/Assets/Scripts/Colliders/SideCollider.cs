﻿using UnityEngine;
using System.Collections;

public class SideCollider : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D other) {
		SendMessageUpwards("OnSideCollide", other, SendMessageOptions.DontRequireReceiver);
	}
	
	void OnTriggerExit2D(Collider2D other) {
		SendMessageUpwards("OnSideExit", SendMessageOptions.DontRequireReceiver);
	}
}
