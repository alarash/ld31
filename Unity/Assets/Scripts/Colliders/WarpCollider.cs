﻿using UnityEngine;
using System.Collections;

public class WarpCollider : MonoBehaviour {
	public GameObject warpTo;
	
	void OnTriggerEnter2D(Collider2D other){
		if(other.GetComponent<Rigidbody2D>() != null){
			GameObject go = other.gameObject;
			go.transform.position = warpTo.transform.position;
		}
	}
}
