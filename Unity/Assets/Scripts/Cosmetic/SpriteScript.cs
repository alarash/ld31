﻿using UnityEngine;
using System.Collections;

public class SpriteScript : MonoBehaviour {

	public int dir = 1;
	
	void Update () {
		Vector3 scale = new Vector3(dir, 1f, 1f);
		transform.localScale = scale;
	}
}
