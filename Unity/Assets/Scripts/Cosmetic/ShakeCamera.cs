﻿using UnityEngine;
using System.Collections;

public class ShakeCamera : MonoBehaviour {

	public float amount = 0f;
	
	Vector3 startPosition;
	
	void Start(){
		startPosition = transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = startPosition + Random.onUnitSphere * amount;
	}
}
