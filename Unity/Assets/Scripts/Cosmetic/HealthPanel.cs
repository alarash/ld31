﻿using UnityEngine;
using System.Collections;

public class HealthPanel : MonoBehaviour {

	public EntityScript entity;
	int lastHealth = -1;

	public GameObject[] hearts;

	
	public void SetHealth(int value){
		for(int i=0; i < hearts.Length; i++){
			hearts[i].SetActive(value > i);
		}
	}
	
	void Update(){
		if(entity.health != lastHealth){
			lastHealth = entity.health;
			SetHealth(entity.health);
		}
	}
}
