﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour {

	EntityScript entityScript;
	
	public float dir = 1f;
	float dirChangeTimer = 0f;
	
	public int damage = 1;
	
	
	void Start(){
		entityScript = GetComponent<EntityScript>();
	}
	
	void Update () {
		if(GameManager.Instance.pause) return;
		if(GameManager.Instance.gameOver) return;
		if(entityScript.health <= 0) return;
		
		
		entityScript.Move(new Vector2(dir, 0));
		
		if(dirChangeTimer > 0){
			dirChangeTimer -= Time.deltaTime;
		}
	}
	
	public void OnCollisionEnter2D(Collision2D other){
		if(other.gameObject.tag == "Player"){
			EntityScript entity = other.gameObject.GetComponent<EntityScript>();
			if(entity.Hurt (damage))
				entity.Push(transform.position, 100f);
		}
	}
	
	public void OnCollisionStay2D(Collision2D other){
		if(other.gameObject.tag == "Player"){
			//entityScript.Move(new Vector2(dir, 0));
			
			EntityScript entity = other.gameObject.GetComponent<EntityScript>();
			if(entity.Hurt (damage))
				entity.Push(transform.position, 100f);
		}
	}
	
	
	void OnEdgeCollide(EdgeTrigger edge){
		switch(edge.type){
			case EdgeType.JUMP:
				if(this.dir == edge.dir){
					entityScript.Jump();
				}
				break;
			
			case EdgeType.WALL:
				this.dir = edge.dir;
				break;	
		}
	}
	
	public void OnKilled(){
        //GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
		Destroy(gameObject, 1f);
	}
}
