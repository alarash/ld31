﻿using UnityEngine;
using System.Collections;

public enum SantaState {
	WALK,
	THROW,
	WIN
}

public class SantaScript : MonoBehaviour {
	
	EntityScript entityScript;
	EntityScript playerEntity;
	Animator animator;
	
	public float dir = 1f;
	float dirChangeTimer = 0f;
	
	public int damage = 1;
	
	ShakeCamera shakeCamera;

	public float fallCounter = 0f;
	
	public AudioClip greetings;
	
	public float hurtedTimer = 0f;
	
	
	public GameObject blackGift;
	public float throwTimer = 0f;
	public float throwForce = 100f;
	
	public SantaState state = SantaState.WALK;
	public float stateTimer = 0f;
	
	
	void Start(){
		entityScript = GetComponent<EntityScript>();
		playerEntity = PlayerScript.Instance.GetComponent<EntityScript>();
		animator = GetComponentInChildren<Animator>();
		shakeCamera = Camera.main.GetComponent<ShakeCamera>();
		
		GetComponent<AudioSource>().clip = greetings;
		GetComponent<AudioSource>().Play();
	}
	
	void Update () {
		if(GameManager.Instance.pause) return;
		
		if(shakeCamera.amount > 0){
			shakeCamera.amount -= Time.deltaTime * 10f;
			if(shakeCamera.amount < 0) shakeCamera.amount = 0;
		}
		
		if(!entityScript.onGround){
			fallCounter += Time.deltaTime;
		}
		
		if(GameManager.Instance.gameOver){
			state = SantaState.WIN;
			//return;
		}
		if(entityScript.health <= 0) return;
		
		switch(state){
			case SantaState.WALK:
				UpdateWalk();
				break;
			
			case SantaState.THROW:
				UpdateThrow();
				break;
				
			case SantaState.WIN:
				UpdateWin();
				break;
		}
	}

	void UpdateWalk ()
	{
		stateTimer += Time.deltaTime;
		if(stateTimer > 6f && entityScript.onGround){
			stateTimer = 0f;
			state = SantaState.THROW;
		}
		
		entityScript.Move(new Vector2(dir, 0));
		
		if(dirChangeTimer > 0){
			dirChangeTimer -= Time.deltaTime;
		}
		
		if( hurtedTimer > 0 && entityScript.onGround){
			hurtedTimer -= Time.deltaTime;
			entityScript.Move(new Vector2(dir * hurtedTimer * 40f, 0));
		}
	}	

	void UpdateThrow ()
	{
		stateTimer += Time.deltaTime;
		if(stateTimer > 2f){
			stateTimer = 0f;
			state = SantaState.WALK;
		}
		
		throwTimer += Time.deltaTime;
		if(throwTimer > 0.5f){
			throwTimer = 0f;
			Throw();
		}		
	}	

	void UpdateWin ()
	{
		animator.SetBool("Win", true);
		
		stateTimer += Time.deltaTime;
		if(stateTimer > 0.5f){
			stateTimer = 0f;
			
			dir = Random.Range(0, 2) == 0 ? -1 : 1;
			entityScript.Move(new Vector2(dir, 0));
		}
	}

	void Throw ()
	{
		dir = Random.Range(0, 2) == 0 ? -1 : 1;
		entityScript.Move(new Vector2(dir, 0));
		
		animator.SetTrigger("Throw");
		
		GameObject go = Instantiate(blackGift, transform.position + Vector3.up * 24f, Quaternion.identity) as GameObject;
		Rigidbody2D rb = go.GetComponent<Rigidbody2D>();
		
		Vector3 throwDir = Random.onUnitSphere;
		rb.AddForce(new Vector2(Mathf.Abs(throwDir.x)*dir, Mathf.Abs(throwDir.y)) * throwForce, ForceMode2D.Impulse);

        
		
	}
	
	public void OnCollisionEnter2D(Collision2D other){
        if (entityScript.health <= 0) return;

		if(other.gameObject.tag == "Player"){
			EntityScript entity = other.gameObject.GetComponent<EntityScript>();
			if(entity.Hurt (damage))
				entity.Push(transform.position, 200f);
		}
	}
	
	void OnEdgeCollide(EdgeTrigger edge){
		switch(edge.type){
		case EdgeType.JUMP:
			if(this.dir == edge.dir){
				entityScript.Jump();
			}
			break;
			
		case EdgeType.WALL:
			this.dir = edge.dir;
			break;
		}
		
	}
	
	void OnGroundCollide(Collider2D other){
        if (entityScript.health <= 0) return;

        if (GetComponent<Rigidbody2D>().velocity.y < -30) return;
		
		if(fallCounter > 0.8f){
			shakeCamera.amount = 5f;
			
			if(playerEntity.onGround){
				if(playerEntity.Hurt(1))
					playerEntity.Push(playerEntity.transform.position - Vector3.up, 200f);
				
			}
			
			stateTimer = 0f;
			state = SantaState.THROW;
		}
		
		fallCounter = 0f;
		
		
	}
	
	void OnFall(){

	}
	
	void OnHurted(){
		hurtedTimer = 0.4f;
		state = SantaState.WALK;
	}
	
	public void OnKilled(){
		GameManager.Instance.YouWin();
	}
}
