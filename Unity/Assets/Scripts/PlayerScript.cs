﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	public static PlayerScript Instance;
	
	EntityScript entityScript;
	Animator anim;
	
	public int score = 0;
	
	public float rage = 0f;
	
	public AudioClip rageOn;
	public AudioClip rageOff;
	
	void Awake(){
		Instance = this;
	}
	
	void Start(){
		entityScript = GetComponent<EntityScript>();
		anim = GetComponentInChildren<Animator>();
	}
	
	void Update(){
		if(GameManager.Instance.pause) return;
		if(GameManager.Instance.gameOver) return;
		if(entityScript.health <= 0) return;
		
		float horizontal = Input.GetAxisRaw("Horizontal");		
		bool up = Input.GetKeyDown(KeyCode.UpArrow);
		
		entityScript.Move(new Vector2(horizontal, 0));
		if(up){
			entityScript.Jump();
		}
		
		anim.SetFloat("Rage", rage);
		if(rage > 0){
			rage -= Time.deltaTime;
			if(rage < 0){
				GetComponent<AudioSource>().clip = rageOff;
				GetComponent<AudioSource>().Play ();
				rage = 0;
			}
		}
	}
	
	public void AddToScore(int value){
		score += value;
		GameManager.Instance.UpdateScore();
	}
	
	public void OnKilled(){
		GameManager.Instance.GameOver();
	}

	public void Rage (float rage)
	{
		if(rage > 0 && this.rage == 0){
			GetComponent<AudioSource>().clip = rageOn;
			GetComponent<AudioSource>().Play();
		}
		this.rage = rage;
	}
	
	public void OnGroundCollide(Collider2D other){
		if(other.tag == "Enemy" && this.rage > 0){
			entityScript.SetNoHurt();
			
			EntityScript entity = other.GetComponent<EntityScript>();
			if(entity == null) return;
			entity.Hurt(1);

			Vector2 vel = GetComponent<Rigidbody2D>().velocity;
			if(vel.y < 0)
				vel.y = 0;
			vel.y += 400f;
			GetComponent<Rigidbody2D>().velocity = vel;			
		}
	}
	
	public void ForceIdle(){
		rage = 0f;
		anim.SetFloat("Rage", 0f);
	}
}
