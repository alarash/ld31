﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

//=====================================================================
[System.Serializable]
public class Level {
	public float spawnTimeStart = 1f;
	public float spawnTimeMin = 1f;
	public float spawnTimeMax = 2f;
	
	public GameObject[] enemies;
	public GameObject[] powerups;
	
	public float timer = 30f;
	
	public float GetSpawnTime(){
		return Random.Range(spawnTimeMin, spawnTimeMax);
	}
	
	public GameObject GetEnemy(){
		if(enemies.Length == 0) return null;
		return enemies[Random.Range(0, enemies.Length)];
	}
	
	public GameObject GetPowerUp(){
		if(powerups.Length == 0) return null;
		
		int rnd = Random.Range(0, powerups.Length);
		
		//QUICK FIX TO NOT SPAWN HEART WHEN PLAYER MAX HEALTH
		EntityScript playerEntity = PlayerScript.Instance.GetComponent<EntityScript>();
		if(playerEntity.health == 5 && rnd == 1)
			rnd += 1;
			
		GameObject go = powerups[rnd];
		
		return go;
	}
}

//=====================================================================
public class GameManager : MonoBehaviour {
	public static GameManager Instance;

	public bool started = false;
	public bool pause = false;
	public bool gameOver = false;
	
	//PLAYER
	public PlayerScript player;

	//SPAWERS
	public Transform[] EnemySpawners;
	public float enemySpawnTimer;
	
	public Transform[] PowerUpSpawners;
	int lastPowerUpIndex = -1;
	
	//LEVELS
	public Level[] levels;
	public int currentLevel = 0;
	public float levelTimer = 0f;
	
	//GUI
	public GameObject titlePanel;
	public GameObject gameOverPanel;
	public Text scoreText;
	public GameObject bossWarning;
	public GameObject youWinPanel;
	
	//=====================================================================
	void Awake() {
		Instance = this;
	}
	
	// Use this for initialization
	void Start () {
		player.enabled = false;
		GetComponent<AudioSource>().volume = 0.3f;
		
		if(PlayerPrefs.HasKey("SCORE")){
			scoreText.text = "BEST: " + PlayerPrefs.GetInt("SCORE");
		}
		else {
			scoreText.text = "";
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!started){
			if(Input.GetKeyDown(KeyCode.Space))
				StartGame();
			return;
		}
		
		if(gameOver){
			if(Input.GetKeyDown(KeyCode.Space)){
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
				GameManager.Instance.started = true;
			}
			return;
		}
		
		if(Input.GetKeyDown(KeyCode.Escape)){
			SetPause(!pause);
		}
		
		if(pause) return;
		
		//ENEMY SPAWNER
		enemySpawnTimer -= Time.deltaTime;
		if(enemySpawnTimer < 0f){
			SpawnEnemy();
		}
		
		//LEVEL
		levelTimer += Time.deltaTime;
		if(levelTimer > CurrentLevel().timer){
			levelTimer = 0f;
			LevelUp();
		}
	}
	
	//=====================================================================
	void StartGame(){
		titlePanel.SetActive(false);
		
		enemySpawnTimer = levels[currentLevel].spawnTimeStart;
		
		UpdateScore();
		
		SpawnPowerUp();
		
		started = true;
		player.enabled = true;
		
		GetComponent<AudioSource>().volume = 1f;
		GetComponent<AudioSource>().Play();
	}
	
	public void GameOver(){
		UpdateMaxScore();
		gameOver = true;
		if(bossWarning != null) bossWarning.SetActive(false);
		gameOverPanel.SetActive(true);
	}

	public void YouWin ()
	{
		UpdateMaxScore();
		GetComponent<AudioSource>().volume = 0.5f;
		
		gameOver = true;
		youWinPanel.SetActive(true);
		
		player.ForceIdle();
		Camera.main.GetComponent<ShakeCamera>().amount = 0f;
	}	
	
	
	void SetPause(bool value){
		pause = value;
		Time.timeScale = value ? 0: 1;
		GetComponent<AudioSource>().volume = value ? 0.3f: 1f;
	}

	void UpdateMaxScore ()
	{
		int lastScore = PlayerPrefs.GetInt("SCORE", 0);
		if(player.score > lastScore)
			PlayerPrefs.SetInt("SCORE", player.score);
	}	

	
	Level CurrentLevel(){
		return levels[currentLevel];
	}
	
	public void LevelUp(){
		if(currentLevel == levels.Length - 1) return; 
		currentLevel ++;
		
		enemySpawnTimer = levels[currentLevel].spawnTimeStart;
		
		if(currentLevel == levels.Length -1){
			bossWarning.SetActive(true);
			Destroy(bossWarning, 3f);
		}
		
		
	}
	
	
	public void SpawnEnemy ()
	{
		enemySpawnTimer = CurrentLevel().GetSpawnTime();
		GameObject enemy = CurrentLevel().GetEnemy();
		if(enemy == null) return;

		Transform spawner = EnemySpawners[Random.Range(0, EnemySpawners.Length)];
		GameObject go = Instantiate(enemy, spawner.position, Quaternion.identity) as GameObject;
		EnemyMove enemyMove = go.GetComponent<EnemyMove>();
		if(enemyMove != null)
			enemyMove.dir = Random.Range(0, 2) == 0 ? 1: -1; 
	}
	
	public void SpawnPowerUp(){
		GameObject powerUp = CurrentLevel().GetPowerUp();
		if(powerUp == null) return;
		
		
		int index;
		while(true){
			index = Random.Range(0, PowerUpSpawners.Length);
			if(index != lastPowerUpIndex){
				lastPowerUpIndex = index;
				break;
			}
		}
		
		Transform spawner = PowerUpSpawners[index];
		Instantiate(powerUp, spawner.position, Quaternion.identity);
	}
	
	public void UpdateScore(){
		scoreText.text = player.score.ToString();
	}
}
