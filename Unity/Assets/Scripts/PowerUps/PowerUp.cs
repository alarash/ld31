﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {	
	public AudioClip grabAudio;
	public int points = 1;
	
	new public string name;
	public string description;

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag != "Player") return;
		
		GetComponent<SpriteRenderer>().enabled = false; 
		GetComponent<AudioSource>().PlayOneShot(grabAudio);
		enabled = false;
		Destroy(gameObject, 1f);
		GetComponent<Collider2D>().enabled = false;
		
		PlayerScript.Instance.AddToScore(points);
		SendMessage("PickedUp", other.gameObject, SendMessageOptions.DontRequireReceiver);
		GameManager.Instance.SpawnPowerUp();
		
		PowerUpPanel.Instance.ShowPowerUp(this);
	}
}
