﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PowerUpPanel : MonoBehaviour {
	public static PowerUpPanel Instance;

	public Text title;
	public Text subtitle;
	
	float timer = 0f;
	
	public void Awake(){
		Instance = this;
	}
	
	public void Update(){
		if(timer > 0){
			timer -= Time.deltaTime;
			if(timer < 0f){
				timer = 0;
				HidePowerUp();
			}
		}
	}
	
	public void ShowPowerUp(PowerUp p){
		title.enabled = true;
		subtitle.enabled = true;
		
		title.text = p.name;
		subtitle.text = p.description;
	
		timer = 2f;
	}

	void HidePowerUp ()
	{
		title.enabled = false;
		subtitle.enabled = false;
	}
}
