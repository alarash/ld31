﻿using UnityEngine;
using System.Collections;

public class PowerUpRage : MonoBehaviour {
	public float rage = 5f;

	void PickedUp(GameObject go){
		go.GetComponent<PlayerScript>().Rage(rage);
	}
}
