﻿using UnityEngine;
using System.Collections;

public class BlackGift : MonoBehaviour {
	Animator animator;
	
	bool explosed = false;
	
	void Start () {
		animator = GetComponent<Animator>();
	}
	
	void OnTriggerEnter2D(Collider2D other){
		if(explosed) return;
		explosed = true;

        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
		
		other.gameObject.SendMessage("Hurt", 1, SendMessageOptions.DontRequireReceiver);
		
		animator.SetTrigger("Boom");
		GetComponent<AudioSource>().Play();
		
		Destroy(gameObject, 1f);
	}
}
