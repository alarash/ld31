﻿using UnityEngine;
using System.Collections;

public class PowerUpHeart : MonoBehaviour {
	public int health = 1;
	
	void PickedUp(GameObject go){
		go.GetComponent<EntityScript>().Heal(health);
	}
}
